﻿using System;

namespace CircularList
{
	interface IAlgorithm<T> where T : IComparable
	{
		void Sort();
		Node<T> BinarySearch(Node<T> searchedValue);
		void Swap(int index1, int index2);
	}
}
