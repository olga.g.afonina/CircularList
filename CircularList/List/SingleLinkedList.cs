﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CircularList
{
	public class SingleLinkedList<T> : IAlgorithm<T>, IEnumerable<Node<T>>
		where T : IComparable
	{
		#region Поля
		Node<T> head;
		Node<T> tail;
		int count;
		#endregion

		#region Свойства
		public int Count => count;

		public bool IsEmpty => count == 0 && head == null && tail == null;
		#endregion

		#region Индексаторы
		public Node<T> this[int index]
		{
			get
			{
				if (index < 0 || index >= count)
					throw new IndexOutOfRangeException("Index");

				int n = 0;

				foreach (var item in this)
				{
					if (index == n)
						return item;
					n++;
				}

				return null;
			}

			set
			{
				if (index < 0 || index >= count)
					throw new IndexOutOfRangeException("Index");

				int n = 0;

				foreach (var item in this)
				{
					if (index == n)
					{
						item.Data = value.Data;
					}
					n++;
				}
			}
		}
		#endregion

		#region Методы - основные операции с элементами списка
		// Вставка
		public void Add(T data)
		{
			var node = new Node<T>(data);
			// Список пуст
			if (IsEmpty)
			{
				head = node;
				tail = node;
				tail.Next = head;
			}
			else
			{
				tail.Next = node;
				node.Next = head;
				tail = node;
			}
			count++;
		}

		// Вставка по индексу - элемент занимает место с указанным индексом
		public void Add(int index, T data)
		{
			var node = new Node<T>(data);
			Node<T> current = head;
			Node<T> prev = null;

			// Индекс за пределами списка
			if (index < 0 || index > count)
				throw new IndexOutOfRangeException("Index");

			// Список пуст
			if (IsEmpty)
			{
				head = node;
				tail = node;
				tail.Next = head;
			}
			else
			{
				int n = 0;

				foreach (var item in this)
				{
					if (n == index)
					{
						if (index == 0) // вместо head
						{
							head = node;
							tail.Next = head;
							head.Next = current;
						}
						else // в середине
						{
							prev.Next = node;
							prev.Next.Next = current;
						}
						break;
					}
					prev = current;
					current = current.Next;
					n++;
				}

				// Индекс равен количеству элементов - вставка в хвост
				if (index == count)
				{
					prev.Next = node;
					tail = node;
					tail.Next = head;
					
				}
			}
			count++;
		}

		// Удаление по узлу
		public bool RemoveByNode(T data)
		{
			if (IsEmpty) return false;

			Node<T> current = head;
			Node<T> prev = null;
			do
			{
				if (current.Data.Equals(data))
				{
					if (prev != null)
					{
						prev.Next = current.Next;

						if (current == tail)
							tail = prev;
					}
					else // если удаляется первый элемент
					{

						// если в списке всего один элемент
						if (count == 1)
						{
							head = tail = null;
						}
						else
						{
							head = current.Next;
							tail.Next = current.Next;
						}
					}
					count--;
					return true;
				}
				prev = current;
				current = current.Next;

			} while (current != head);

			return false;
		}

		// Удаление по индексу
		public bool RemoveByIndex(int index)
		{
			if (IsEmpty) return false;

			if (index < 0 || index >= count)
				throw new IndexOutOfRangeException("Index");

			Node<T> current = head;
			Node<T> prev = null;
			int n = 0;

			do
			{
				// Узел найден
				if (n == index)
				{
					if (prev == null) // первый узел
					{
						if (count == 1)
							head = tail = null;
						else
						{
							head = current.Next;
							tail.Next = current.Next;
						}
					}
					else // Последний или в середине
					{
						// Последний
						if (current == tail)
							tail = prev;
						else  // В середине
						{
							tail.Next = current.Next;
							head = current.Next;
						}
					}
					count--;
					return true;
				}
				n++;
				prev = current;
				current = current.Next;

			} while (current != head);

			return false;
		}

		// Очистка списка
		public void Clear()
		{
			head = tail = null;
			count = 0;
		}

		// Содержит ли заданный элемент
		public bool Contains(T data)
		{
			if (IsEmpty) return false;

			foreach (var item in this)
			{
				if (item.Data.Equals(data))
					return true;
			}
			return false;
		}

		// Вывод на консоль
		public void Display()
		{
			if (IsEmpty)
			{
				Console.WriteLine("Список пуст");
			}
			else
			{
				int n = 0;
				foreach (var item in this)
				{
					Console.Write($"[{item.Data.ToString()}]");
					if (n != count - 1)
					{
						Console.Write(" -> ");
					}
					n++;
				}
				Console.WriteLine();
			}
		}
		#endregion

		#region Реализация интерфейса IEnumerable
		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable)this).GetEnumerator();
		}

		IEnumerator<Node<T>> IEnumerable<Node<T>>.GetEnumerator()
		{
			Node<T> current = head;
			do
			{
				if (current != null)
				{
					yield return current;
					current = current.Next;
				}
			}
			while (current != head);
		}
		#endregion

		#region Реализация интерфейса IAlgorithm
		// Обмен местами двух элементов
		public void Swap(int index1, int index2)
		{
			var list = this;
			var tmp = list[index1].Data;
			list[index1].Data = list[index2].Data;
			list[index2].Data = tmp;
		}

		public void Sort()
		{
			QuickSort(this, 0, Count - 1);
		}

		private void QuickSort(SingleLinkedList<T> list, int start, int end)
		{
			if (start >= end)
			{
				return;
			}
			int pivot = Part(list, start, end);
			QuickSort(list, start, pivot - 1);
			QuickSort(list, pivot + 1, end);
		}

		private int Part(SingleLinkedList<T> list, int start, int end)
		{
			int marker = start;
			int n = 0;

			foreach (var l in list)
			{
				if (n >= start && n < end)
				{
					var nodeI = list[n];
					var nodeEnd = list[end];
					if (nodeI.Data.CompareTo(nodeEnd.Data) == -1) //if (array[i] < array[end])
					{
						list.Swap(marker, n);
						marker += 1;
					}
				}
				n++;
			}

			list.Swap(marker, end);

			return marker;
		}

		// Бинарный поиск в отсортированном списке
		private Node<T> BinarySearch(Node<T> searchedValue, int left, int right)
		{
			var list = this;
			// Пока не сошлись границы массива
			while (left <= right)
			{
				// Индекс среднего элемента
				var middle = (left + right) / 2;

				if (searchedValue.Data.Equals(list[middle].Data))
				{
					return list[middle];
				}
				else if (searchedValue.Data.CompareTo(list[middle].Data) == -1)
				{
					// Сужаем рабочую зону массива с правой стороны
					right = middle - 1;
				}
				else
				{
					// Сужаем рабочую зону массива с левой стороны
					left = middle + 1;
				}
			}
			// Ничего не нашли
			return null;
		}

		public Node<T> BinarySearch(Node<T> searchedValue)
		{
			return BinarySearch(searchedValue, 0, Count - 1);
		}
		#endregion
	}
}
