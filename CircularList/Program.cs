﻿using System;

namespace CircularList
{
	class Person : IComparable
	{
		public string FirstName { get; set; }

		public string LastName { get; set; }

		public DateTime DateOfBirth { get; set; }

		public override string ToString()
		{
			return $"{LastName} {FirstName} {DateOfBirth.ToShortDateString()}";
		}

		public int CompareTo(object obj)
		{
			if (DateOfBirth > ((Person)obj).DateOfBirth)
				return 1;
			if (DateOfBirth < ((Person)obj).DateOfBirth)
				return -1;
			else
				return 0;
		}

		public Person(string firstName, string lastName, DateTime dateOfBirth)
		{
			FirstName = firstName;
			LastName = lastName;
			DateOfBirth = dateOfBirth;
		}

		public override bool Equals(object obj)
		{
			var person = obj as Person;
			if (person == null) return false;
			return person.FirstName == FirstName && person.LastName == LastName && person.DateOfBirth == DateOfBirth;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
	class Program
	{
		static void Main()
		{
			var dashString = new string('-', 120);
			var starString = new string('*', 120);
			Console.WriteLine("\t\t\t\t\tОдносвязный кольцевой список");
			Console.WriteLine(starString);
			Console.WriteLine("Исходный список");
			Console.WriteLine(dashString);
			var persons = new SingleLinkedList<Person> {
				new Person("Иванов", "Иван", new DateTime(1980, 1, 1)),
				new Person("Петров", "Петр", new DateTime(1985, 2, 1)),
				new Person("Афонина", "Ольга", new DateTime(1983, 12, 31)),
				new Person("Сидоров", "Александр", new DateTime(1990, 3, 1)),
				new Person("Леонов", "Игорь", new DateTime(1962, 4, 1)),
				new Person("Смирнов", "Леонид", new DateTime(1975, 5, 1)),
				new Person("Лебедев", "Денис", new DateTime(1965, 6, 1)),
				new Person("Савельева", "Ирина", new DateTime(1968, 6, 9))
			};
			persons.Display();
			Console.WriteLine(starString);
			Console.WriteLine();

			Console.WriteLine("Вставка элемента: Митрофанов Николай 24.05.1992 в конец списка");
			Console.WriteLine(dashString);
			persons.Add(new Person("Николай", "Митрофанов", new DateTime(1992, 5, 24)));
			persons.Display();
			Console.WriteLine(starString);
			Console.WriteLine();

			Console.WriteLine("Вставка элемента: Семенов Георгий 20.08.1956 по индексу 2");
			Console.WriteLine(dashString);
			persons.Add(2, new Person("Семенов", "Георгий", new DateTime(1956, 8, 20)));
			persons.Display();
			Console.WriteLine(starString);
			Console.WriteLine();

			Console.WriteLine("Доступ к элементу по индексу 5");
			Console.WriteLine(dashString);
			Console.WriteLine($"Элемент: {persons[5].Data.ToString()}");
			Console.WriteLine(starString);
			Console.WriteLine();

			Console.WriteLine("Изменение элемента по индексу 5");
			Console.WriteLine(dashString);
			persons[5].Data = new Person("Смирнова", "Маргарита", new DateTime(1986, 2, 3));
			Console.WriteLine($"Элемент: {persons[5].Data.ToString()}");
			Console.WriteLine(starString);
			Console.WriteLine();
			
			Console.WriteLine("Удаление элемента: Иванов Иван 01.01.1980, потом элемента с индексом 0");
			Console.WriteLine(dashString);
			persons.RemoveByNode(new Person("Иванов", "Иван", new DateTime(1980, 1, 1)));
			persons.RemoveByIndex(0);
			persons.Display();
			Console.WriteLine(starString);
			Console.WriteLine();

			Console.WriteLine("Сортировка по дате рождения");
			Console.WriteLine(dashString);
			persons.Sort();
			persons.Display();
			Console.WriteLine(starString);
			Console.WriteLine();

			Console.WriteLine("Бинарный поиск элемента: Афонина Ольга 31.12.1983");
			Console.WriteLine(dashString);
			var res = persons.BinarySearch(new Node<Person>(new Person("Афонина", "Ольга", new DateTime(1983, 12, 31))));
			Console.WriteLine("Найдено: " + res.Data.ToString());
			Console.WriteLine(starString);
			Console.ReadLine();
		}
	}
}
