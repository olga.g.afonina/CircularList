using NUnit.Framework;
using CircularList;
using System;

namespace CircularListTests
{
	public class Tests
	{
		// ������� � ������ ������
		[Test]
		public void Add_IfListEmpty()
		{
			// Arrange
			var list = new SingleLinkedList<int>();

			// Act
			list.Add(1);

			// Assert
			Assert.AreEqual(1, list.Count);
		}

		// ������� ���������� ��������� � ������
		[Test]
		public void Add_SeveralElements()
		{
			// Arrange
			var list = new SingleLinkedList<int>();

			// Act
			list.Add(1);
			list.Add(2);
			list.Add(3);

			// Assert
			Assert.AreEqual(3, list.Count);
			Assert.AreEqual(list[0].Data, 1);
			Assert.AreEqual(list[1].Data, 2);
			Assert.AreEqual(list[2].Data, 3);
		}

		// ������������ ������
		[Test]
		public void AddByIndex_throwIndexOutOfRangeException()
		{
			// Arrange
			var list = new SingleLinkedList<int>();

			// Arrange
			Assert.Throws<IndexOutOfRangeException>(() => list.Add(1, 1));
		}

		// ������� �� ������� � ������ ������
		[Test]
		public void AddByIndex_IfListIsEmpty()
		{
			// Arrange
			var list = new SingleLinkedList<int>();

			// Act
			list.Add(list.Count, 1);

			// Assert
			Assert.AreEqual(1, list.Count);
			Assert.AreEqual(1, list[0].Data);
		}

		// ������� ���������� ��������� �� ������� (� ������)
		[Test]
		public void AddByIndex_Head()
		{
			// Arrange
			var list = new SingleLinkedList<int>();

			// Act 
			list.Add(0, 1);
			list.Add(0, 2);
			list.Add(0, 3);

			//Assert
			Assert.AreEqual(3, list.Count);
			Assert.AreEqual(3, list[0].Data);
			Assert.AreEqual(2, list[1].Data);
			Assert.AreEqual(1, list[2].Data);
		}

		// ������� ���������� ��������� �� ������� (� ��������)
		[Test]
		public void AddByIndex_Middle()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5 };

			// Act 
			list.Add(1, 10);
			list.Add(1, 11);
			list.Add(1, 12);

			//Assert
			Assert.AreEqual(8, list.Count);
			Assert.AreEqual(12, list[1].Data);
			Assert.AreEqual(11, list[2].Data);
			Assert.AreEqual(10, list[3].Data);
		}

		// ������� ���������� ��������� �� ������� (� �����)
		// ����������� ������� ���������� tail
		[Test]
		public void AddByIndex_Tail()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5 };

			// Act 
			list.Add(list.Count, 10);
			list.Add(list.Count, 11);
			list.Add(list.Count, 12);

			//Assert
			Assert.AreEqual(8, list.Count);
			Assert.AreEqual(10, list[5].Data);
			Assert.AreEqual(11, list[6].Data);
			Assert.AreEqual(12, list[7].Data);
		}

		// �������� - ������ ����
		[Test]
		public void RemoveByNode_IfListEmpty()
		{
			// Arrange
			var list = new SingleLinkedList<int>();

			// Act 
			var result = list.RemoveByNode(10);

			//Assert
			Assert.IsFalse(result);
			Assert.AreEqual(0, list.Count);
		}

		// �������� ������������� �������� � ������
		[Test]
		public void RemoveByNode_SingleElementInList()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1 };

			// Act 
			var result = list.RemoveByNode(1);

			//Assert
			Assert.IsTrue(result);
			Assert.AreEqual(0, list.Count);
		}

		// �������� - ���������� �������� �� ����������
		[Test]
		public void RemoveByNode_IfElementNotExists()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1 };

			// Act 
			var result = list.RemoveByNode(2);

			//Assert
			Assert.IsFalse(result);
			Assert.AreEqual(1, list.Count);
		}

		// �������� ������� ��������
		[Test]
		public void RemoveByNode_Head()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3 };

			// Act 
			var result = list.RemoveByNode(1);

			//Assert
			Assert.IsTrue(result);
			Assert.AreEqual(2, list.Count);
		}

		// �������� �� �������� ������
		[Test]
		public void RemoveByNode_Middle()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5 };

			// Act 
			var result = list.RemoveByNode(3);

			//Assert
			Assert.IsTrue(result);
			Assert.AreEqual(4, list.Count);
		}

		// �������� ���������� ��������
		[Test]
		public void RemoveByNode_Tail()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5 };

			// Act 
			var result = list.RemoveByNode(5);

			//Assert
			Assert.IsTrue(result);
			Assert.AreEqual(4, list.Count);
		}

		// �������� - ������ ����
		[Test]
		public void RemoveByIndex_IfListEmpty()
		{
			// Arrange
			var list = new SingleLinkedList<int>();

			// Act 
			var result = list.RemoveByIndex(10);

			//Assert
			Assert.IsFalse(result);
			Assert.AreEqual(0, list.Count);
		}

		// �������� ������������� �������� � ������
		[Test]
		public void RemoveByIndex_SingleElementInList()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1 };

			// Act 
			var result = list.RemoveByIndex(0);

			//Assert
			Assert.IsTrue(result);
			Assert.AreEqual(0, list.Count);
		}

		// �������� - ���������� �������� �� ����������
		[Test]
		public void RemoveByIndex_throwIndexOutOfRangeException()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1 };

			//Assert
			Assert.Throws<IndexOutOfRangeException>(() => list.RemoveByIndex(1));
		}

		// �������� ������� ��������
		[Test]
		public void RemoveByIndex_Head()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3 };

			// Act 
			var result = list.RemoveByIndex(0);

			//Assert
			Assert.IsTrue(result);
			Assert.AreEqual(2, list.Count);
		}

		// �������� �� �������� ������
		[Test]
		public void RemoveByIndex_Middle()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5 };

			// Act 
			var result = list.RemoveByIndex(2);

			//Assert
			Assert.IsTrue(result);
			Assert.AreEqual(4, list.Count);
		}

		// �������� ���������� ��������
		[Test]
		public void RemoveByIndex_Tail()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5 };

			// Act 
			var result = list.RemoveByIndex(4);

			//Assert
			Assert.IsTrue(result);
			Assert.AreEqual(4, list.Count);
		}

		// ������� ������
		[Test]
		public void Clear()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5 };

			// Act 
			list.Clear();

			//Assert
			Assert.AreEqual(0, list.Count);
			Assert.IsTrue(list.IsEmpty);
		}

		[Test]
		public void Contains_IfListEmpty()
		{
			// Arrange
			var list = new SingleLinkedList<int>();

			// Act 
			var result = list.Contains(1);

			//Assert
			Assert.AreEqual(0, list.Count);
			Assert.IsFalse(result);
		}

		[Test]
		public void Contains_IfElementExists()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5 };

			// Act 
			var result = list.Contains(1);

			//Assert
			Assert.IsTrue(result);
		}

		[Test]
		public void Contains_IfElementNotExists()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5 };

			// Act 
			var result = list.Contains(6);

			//Assert
			Assert.IsFalse(result);
		}

		[Test]
		public void Getter_throwIndexOutOfRangeException()
		{
			Assert.Throws<IndexOutOfRangeException>(delegate
			{
				var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5 };
				var result = list[5];
			});
		}

		[Test]
		public void Getter_throwIndexOutOfRangeException_IfListEmpty()
		{
			Assert.Throws<IndexOutOfRangeException>(delegate
			{
				var list = new SingleLinkedList<int>();
				var result = list[0];
			});
		}

		[Test]
		public void Setter_throwIndexOutOfRangeException()
		{
			Assert.Throws<IndexOutOfRangeException>(delegate
			{
				var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5 };
				list[5] = new Node<int>(10);
			});
		}

		[Test]
		public void Setter_throwIndexOutOfRangeException_IfListEmpty()
		{
			Assert.Throws<IndexOutOfRangeException>(delegate
			{
				var list = new SingleLinkedList<int>();
				list[0] = new Node<int>(10);
			});
		}

		[Test]
		public void Getter()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5 };

			// Act
			var result = list[0];

			// Assert
			Assert.AreEqual(1, result.Data);
		}

		[Test]
		public void Setter()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5 };

			// Act
			list[4] = new Node<int>(10);

			// Assert
			Assert.AreEqual(10, list[4].Data);
		}

		[Test]
		public void Sort_IfListEmpty()
		{
			// Arrange
			var list = new SingleLinkedList<int>();

			// Act
			list.Sort();

			// Assert
			Assert.AreEqual(0, list.Count);
		}

		[Test]
		public void Sort()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 5, 4, 3, 2, 1 };

			// Act
			list.Sort();

			// Assert
			Assert.AreEqual(1, list[0].Data);
			Assert.AreEqual(2, list[1].Data);
			Assert.AreEqual(3, list[2].Data);
			Assert.AreEqual(4, list[3].Data);
			Assert.AreEqual(5, list[4].Data);
		}

		[Test]
		public void Swap()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 5, 4, 3, 2, 1 };

			// Act
			list.Swap(0, list.Count - 1);

			// Assert
			Assert.AreEqual(1, list[0].Data);
			Assert.AreEqual(5, list[4].Data);
		}

		[Test]
		public void Swap_throwIndexOutOfRangeException()
		{
			// Arrange
			var list = new SingleLinkedList<int>();

			Assert.Throws<IndexOutOfRangeException>(() => list.Swap(1, 1));
		}

		[Test]
		public void BinarySearch()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

			// Act
			var result = list.BinarySearch(new Node<int>(5));

			// Assert
			Assert.AreEqual(5, result.Data);
		}

		[Test]
		public void BinarySearch_NotFound()
		{
			// Arrange
			var list = new SingleLinkedList<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

			// Act
			var result = list.BinarySearch(new Node<int>(50));

			// Assert
			Assert.AreEqual(null, result);
		}
	}
}